package com.example.pong;

import org.andengine.entity.text.Text;

public class Score {
	private int score;
	private String displayScore;
	public Text display;

	Score(float x, float y) {
		init(x, y, false);
	}

	public Score(float x, float y, boolean bottom) {
		init(x, y, bottom);
	}

	private void init(float x, float y, boolean bottom) {
		score = 0;
		displayScore = "Score: ";
		display = new Text(0, 0, MainActivity.getSharedInstance().mFont, displayScore + score, 50, MainActivity.getSharedInstance()
				.getVertexBufferObjectManager());

		adjustScorePosition(x, y, bottom);
	}

	private void adjustScorePosition(float x, float y, boolean bottom) {
		if (!bottom) {
			display.setPosition(x + (display.getWidth() / 2), y + (display.getHeight() / 2));
		} else {
			display.setPosition(x + (display.getWidth() / 2), y - (display.getHeight() / 2));
		}
	}

	public void displayScore(boolean bottom) {
		display.setText(displayScore + score);

		if (!bottom) {
			adjustScorePosition(0, 0, bottom);
		} else {
			adjustScorePosition(0, MainActivity.getSharedInstance().mCamera.getHeight(), bottom);
		}
	}

	public void applyPointsPU(int p) {
		score += p;
	}

	public int getScore() {
		return score;
	}

	public void resetScore() {
		score = 0;
	}
}