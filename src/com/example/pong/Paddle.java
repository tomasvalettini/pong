package com.example.pong;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;

public class Paddle {
	public Rectangle sprite;
	public static Paddle instance;
	private Camera mCamera;
	private boolean moveable;

	public static Paddle getSharedInstance() {
		if (instance == null) {
			instance = new Paddle();
		}

		return instance;
	}

	private Paddle() {
		sprite = new Rectangle(0, 0, 100, 20, MainActivity.getSharedInstance().getVertexBufferObjectManager());
		mCamera = MainActivity.getSharedInstance().mCamera;
		sprite.setPosition(mCamera.getWidth() / 2, sprite.getHeight() + 30);
		moveable = true;
	}

	// public void movePaddle(float accelerometerSpeedX) {
	// if (!moveable) {
	// return;
	// }
	//
	// if (accelerometerSpeedX != 0) {
	// int lL = (int) (sprite.getWidth() / 2);
	// int rL = (int) (mCamera.getWidth() - (int) (sprite.getWidth() / 2));
	// float newX = sprite.getX() + accelerometerSpeedX;
	// float newY = sprite.getY();
	//
	// // Calculate new X, Y coordinates within limits
	// if (newX < lL) {
	// newX = lL;
	// } else if (newX > rL) {
	// newX = rL;
	// }
	//
	// sprite.setPosition(newX, newY);
	// }
	// }

	public void movePaddle(float accelerometerSpeedX) {
		if (!moveable) {
			return;
		}

		int lL = (int) (sprite.getWidth() / 2);
		int rL = (int) (mCamera.getWidth() - (int) (sprite.getWidth() / 2));
		float newX = accelerometerSpeedX;
		float newY = sprite.getY();

		if (newX < lL) {
			newX = lL;
		} else if (newX > rL) {
			newX = rL;
		}

		sprite.setPosition(accelerometerSpeedX, newY);
	}
}