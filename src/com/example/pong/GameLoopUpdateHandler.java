package com.example.pong;

import org.andengine.engine.handler.IUpdateHandler;

public class GameLoopUpdateHandler implements IUpdateHandler {
	private GameScene scene;
	private int CPU_SLEEP = 100;
	float totalElapsed = 0;

	public GameLoopUpdateHandler() {
		scene = ((GameScene) MainActivity.getSharedInstance().mCurrentScene);
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		if (!scene.checkOutOfBounds()) {
			totalElapsed += (pSecondsElapsed * 1000);
			scene.movePaddle();

			if (totalElapsed > CPU_SLEEP) {
				scene.moveCpuPaddle();
				totalElapsed -= CPU_SLEEP;
			}

			scene.moveBall();
			scene.checkCollision();
		} else {
			scene.updateScores();
		}
	}

	@Override
	public void reset() {
	}
}