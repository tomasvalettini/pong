package com.example.pong;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;

public class Ball {
	public Rectangle sprite;
	public static Ball instance;
	private Camera mCamera;

	float dx; // angle of the x coordinate
	float dy; // angle of the y coordinate

	private int SPEED = 5;
	private final int MAX_SPEED = 10;

	private Ball() {
		sprite = new Rectangle(0, 0, 20, 20, MainActivity.getSharedInstance().getVertexBufferObjectManager());
		sprite.setColor(1, 0, 0);
		mCamera = MainActivity.getSharedInstance().mCamera;
		resetBall((float) (Math.PI / 2));
	}

	public void resetBall(float initAngle) {
		// initial position
		sprite.setPosition(mCamera.getWidth() / 2 - sprite.getWidth() / 2, mCamera.getHeight() / 2 - sprite.getHeight() / 2);

		// setting the angle
		dy = initAngle;
		dx = dy;
	}

	public static Ball getSharedInstance() {
		if (instance == null) {
			instance = new Ball();
		}

		return instance;
	}

	public void moveBall() {
		// int bL = (int) sprite.getHeight() / 2;
		// int uL = (int) (mCamera.getHeight() - bL);
		int lL = (int) sprite.getWidth() / 2;
		int rL = (int) (mCamera.getWidth() - lL);
		float newY = sprite.getY() + (float) (Math.round(Math.sin(dy) * SPEED));
		float newX = sprite.getX() + (float) (Math.round(Math.cos(dx) * SPEED));

		// if (newY < bL) {
		// newY = bL;
		// dy *= -1;
		// } else if (newY > uL) {
		// newY = uL;
		// dy *= -1;
		// }

		if (newX < lL) {
			newX = lL;
			dx -= Math.PI;
		} else if (newX > rL) {
			newX = rL;
			dx += Math.PI;
		}

		sprite.setPosition(newX, newY);
	}
	
	public void increaseSpeed()
	{
		SPEED += 1;
		
		if (SPEED > MAX_SPEED)
		{
			SPEED = MAX_SPEED;
		}
	}
}