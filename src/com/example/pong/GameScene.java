package com.example.pong;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;

import android.hardware.SensorManager;

public class GameScene extends Scene {
	Camera mCamera;
	float accelerometerSpeedX;
	SensorManager sensorManager;

	// Game elements
	public Paddle paddle;
	public Ball ball;
	public PaddleCPU cpu;
	public Score scoreCPU;
	public Score scoreHuman;

	private final int points = 100;
	private int numberOfResets = 0;

	public GameScene() {
		// setting the background and saving the activity camera for later use!
		setBackground(new Background(0.09804f, 0.6274f, 0.8784f));
		mCamera = MainActivity.getSharedInstance().mCamera;

		paddle = Paddle.getSharedInstance();
		ball = Ball.getSharedInstance();
		cpu = PaddleCPU.getSharedInstance();
		scoreHuman = new Score(0, 0);
		scoreCPU = new Score(0, mCamera.getHeight(), true);

		attachChild(paddle.sprite);
		attachChild(ball.sprite);
		attachChild(cpu.sprite);
		attachChild(scoreCPU.display);
		attachChild(scoreHuman.display);

		MainActivity.getSharedInstance().setCurrentScene(this);

		// sensorManager = (SensorManager)
		// MainActivity.getSharedInstance().getSystemService(MainActivity.SENSOR_SERVICE);
		// SensorListener.getSharedInstance();
		// sensorManager.registerListener(SensorListener.getSharedInstance(),
		// sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
		// SensorManager.SENSOR_DELAY_GAME);
		setOnSceneTouchListener(MainActivity.getSharedInstance());
		registerUpdateHandler(new GameLoopUpdateHandler());

		paddle.sprite.setPosition(mCamera.getWidth() / 2, paddle.sprite.getY());
	}

	public void movePaddle() {
		paddle.movePaddle(accelerometerSpeedX);
	}

	public void moveCpuPaddle() {
		cpu.movePaddle(ball.sprite.getX(), ball.sprite.getY());
	}

	public void moveBall() {
		ball.moveBall();
	}

	public void checkCollision() {
		if (ball.sprite.collidesWith(paddle.sprite)) {
			// ball.dy *= -1;
			ball.dy = getNewAngle();
			ball.dx = ball.dy;
			ball.sprite.setY(paddle.sprite.getY() + (ball.sprite.getHeight() / 2) + (paddle.sprite.getHeight() / 2));
		}

		if (ball.sprite.collidesWith(cpu.sprite)) {
			// ball.dy *= -1;
			ball.dy = getNewAngle2();
			ball.dx = ball.dy;
			ball.sprite.setY(cpu.sprite.getY() - (ball.sprite.getHeight() / 2) - (cpu.sprite.getHeight() / 2));
		}
	}

	// when the ball hits the player's pad it calculates a new angle to redirect
	// the ball
	private float getNewAngle() {
		float div, num = 2, denum = 4;

		num += ((paddle.sprite.getX() - ball.sprite.getX()) / (paddle.sprite.getWidth() / 2));
		div = (float) (Math.PI * num / denum);

		return div;
	}

	// when the ball hits the player's pad it calculates a new angle to redirect
	// the ball
	private float getNewAngle2() {
		float div, num = 6, denum = 4;

		num -= ((cpu.sprite.getX() - ball.sprite.getX()) / (cpu.sprite.getWidth() / 2));
		div = (float) (Math.PI * num / denum);

		return div;
	}

	public boolean checkOutOfBounds() {
		if (ball.sprite.getY() < 0) {
			scoreCPU.applyPointsPU(points);
			ball.resetBall((float) (Math.PI * 3 / 2));
			numberOfResets++;

			return true;
		} else if (ball.sprite.getY() > mCamera.getHeight()) {
			scoreHuman.applyPointsPU(points);
			ball.resetBall((float) (Math.PI / 2));
			cpu.increaseSpeed();
			numberOfResets++;

			return true;
		}

		if (numberOfResets > 0 && numberOfResets % 5 == 0) {
			ball.increaseSpeed();
			numberOfResets = 0;
		}

		return false;
	}

	public void updateScores() {
		scoreCPU.displayScore(true);
		scoreHuman.displayScore(false);
	}
}