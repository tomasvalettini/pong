package com.example.pong;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import android.graphics.Typeface;
import android.util.Log;

public class MainActivity extends SimpleBaseGameActivity implements IOnSceneTouchListener {
	// vars for the camera
	static final int CAMERA_WIDTH = 480;
	static final int CAMERA_HEIGHT = 800;

	// font and camera
	public Font mFont;
	public Camera mCamera;

	// A reference to the current scene
	public Scene mCurrentScene;
	// singleton instance
	public static MainActivity instance;

	@Override
	public EngineOptions onCreateEngineOptions() {
		instance = this;
		// Creating a new camera for the game!!!
		mCamera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		// returning engine options for the screen orientation, and resolution!!
		return new EngineOptions(true, ScreenOrientation.PORTRAIT_SENSOR, new FillResolutionPolicy(), mCamera);
	}

	@Override
	protected void onCreateResources() {
		// creating font with specific face, attribute (bold or italic) and
		// size!
		mFont = FontFactory.create(this.getFontManager(), this.getTextureManager(), 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32);
		mFont.load();
	}

	@Override
	protected Scene onCreateScene() {
		mEngine.registerUpdateHandler(new FPSLogger());
		// Creating scene and assigning a background color!!
		mCurrentScene = new SplashScreen();
		
		return mCurrentScene;
	}

	public static MainActivity getSharedInstance() {
		return instance;
	}

	public void setCurrentScene(Scene scene) {
		mCurrentScene = scene;
		getEngine().setScene(mCurrentScene);
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		Log.v("finger x:", pSceneTouchEvent.getX() + "");
		((GameScene) pScene).accelerometerSpeedX = pSceneTouchEvent.getX();
		return true;
	}
}