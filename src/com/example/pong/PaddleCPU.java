package com.example.pong;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;

public class PaddleCPU {
	public Rectangle sprite;
	public static PaddleCPU instance;
	private Camera mCamera;
	private boolean moveable;
	private int SPEED = 4;
	private final int MAX_SPEED = 18;

	public static PaddleCPU getSharedInstance() {
		if (instance == null) {
			instance = new PaddleCPU();
		}

		return instance;
	}

	private PaddleCPU() {
		sprite = new Rectangle(0, 0, 100, 20, MainActivity.getSharedInstance().getVertexBufferObjectManager());
		mCamera = MainActivity.getSharedInstance().mCamera;
		sprite.setPosition(mCamera.getWidth() / 2, mCamera.getHeight() - sprite.getHeight() - 30);
		moveable = true;
	}

	public void movePaddle(float ballX, float ballY) {
		if (!moveable) {
			return;
		}

		int lL = (int) (sprite.getWidth() / 2);
		int rL = (int) (mCamera.getWidth() - (int) (sprite.getWidth() / 2));
		float newX = sprite.getX();
		float newY = sprite.getY();
		float half = sprite.getWidth() / 2;

		if (ballY > mCamera.getWidth() / 2) {
			if (ballX < (newX - half)) {
				newX -= SPEED;
			} else if (ballX > (newX + half)) {
				newX += SPEED;
			} else {
				newX += 0;
			}

			// Calculate new X, Y coordinates within limits
			if (newX < lL) {
				newX = lL;
			} else if (newX > rL) {
				newX = rL;
			}

			sprite.setPosition(newX, newY);
		}
	}
	
	public void increaseSpeed()
	{
		SPEED += 2;
		
		if (SPEED > MAX_SPEED)
		{
			SPEED = MAX_SPEED;
		}
	}
}