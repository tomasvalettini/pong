package com.example.pong;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.TextMenuItem;

public class MainMenuScene extends MenuScene implements IOnMenuItemClickListener
{
	MainActivity activity;
	final int MENU_START = 0;
	
	public MainMenuScene()
	{
		super(MainActivity.getSharedInstance().mCamera);
		activity = MainActivity.getSharedInstance();
		
		setBackground(new Background(0.09804f, 0.6274f, 0.8784f));
		IMenuItem startBtn = new TextMenuItem(MENU_START, activity.mFont, activity.getString(R.string.start), activity.getVertexBufferObjectManager());
		startBtn.setPosition(activity.mCamera.getWidth() / 2, activity.mCamera.getHeight() / 2);
		addMenuItem(startBtn);
		
		setOnMenuItemClickListener(this);
	}
	
	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY)
	{
		switch (pMenuItem.getID())
		{
			case MENU_START:
				activity.setCurrentScene(new GameScene());
				return true;
			default:
				break;
		}
		
		return false;
	}
}